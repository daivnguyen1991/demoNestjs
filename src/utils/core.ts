import axios from 'axios';
import buildFileLogger from './winstonLogger';

const core = axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com',
});

const sms = {};
const log = buildFileLogger({});

core.interceptors.request.use(
  function(config) {
    // Do something before request is sent
    const { method, url, data } = config;
    sms['method'] = method;
    sms['url'] = url;
    sms['request'] = data;
    return config;
  },
  function(error) {
    sms['request'] = error;
    // Do something with request error
    return Promise.reject(error);
  },
);

// Add a response interceptor
core.interceptors.response.use(
  function(response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    const { data, status, statusText } = response;
    sms['response'] = { data, status, statusText };
    log.info(sms);
    return response;
  },
  function(error) {
    // sms['response'] = error;
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    sms['response'] = error;
    log.error(sms);
    return Promise.reject({
      statusCode: 501,
      timestamp: new Date().toISOString(),
      message:
        'Chào mừng bạn đến với Hahalolo, API nay đang được bảo trì và nâng cấp, xin bạn thử lại sau!',
    });
  },
);

export default core;
