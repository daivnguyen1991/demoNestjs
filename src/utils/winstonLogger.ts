// eslint-disable-next-line @typescript-eslint/no-var-requires
const winston = require('winston');

const getName = () => {
  const dd = new Date().getDate();
  const mm = new Date().getMonth();
  const year = new Date().getFullYear().toString();
  let date = dd.toString();
  let month = mm.toString();
  if (dd < 10) {
    date = '0' + dd;
  }
  if (mm < 10) {
    month = '0' + mm;
  }
  return date + month + year;
};

export default function buildFileLogger({
  level = 'info',
  maxFiles = undefined,
  maxsize = 20480,
}): any {
  return winston.createLogger({
    format: winston.format.combine(
      winston.format.timestamp(),
      winston.format.json(),
    ),
    level,
    levels: winston.config.syslog.levels,
    transports: [
    //   new winston.transports.Console(),
      new winston.transports.File({
        filename: `./log/log-${getName()}-hl.log`,
        maxsize,
        maxFiles,
      }),
    ],
  });
}
