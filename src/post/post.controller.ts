import { Controller, Get, Post, Body } from '@nestjs/common';
import core from '../utils/core';

@Controller('post')
export class PostController {


  @Get()
  post(): Promise<any> {
    return core('/posts')
      .then(function(response) {
        return response.data;
      })
      .catch(e => e);
  }
  @Get('/err')
  posterr(): Promise<any> {
    return core('/posts-----')
      .then(function(response) {
        return response.data;
      })
      .catch(e => e);
  }
  @Post()
  postdata(
    @Body('title') title: string,
    @Body('body') body: string,
    @Body('userId') userId: number,
  ): Promise<any> {
    return core
      .post('/posts', {
        title,
        body,
        userId,
      })
      .then(function(response) {
        return response.data;
      })
      .catch(e => e);
  }
}
