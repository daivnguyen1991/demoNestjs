## Description

Demo setup nestjs cơ bản cho trang develop.hahalolo.com

Mong muốn đạt được:

- [x] hỗ trợ lấy dữ liệu từ core và không sử lí dữ liệu
- [x] handle respond khi core có lỗi
- [x] handle respond khi route không tồn tại hoặc các common httpException
- [x] Log to file để thuận tiện kiểm tra.
- [x] Hướng dẫn sử trong quá trình build up server

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start
```

## Utils Logger

sử dụng winston logger: https://www.npmjs.com/package/winston

```javascript
// line 33-37
new winston.transports.File({
  filename: `./log/log-${getName()}-hl.log`,
  maxsize, //20480
  maxFiles,
}),
```

file log sẽ được lưu trong thư mục log với định dạng như trên

- vd: log-31062020-hl.log
  - 31062020: ý nghĩa ngày 31 tháng 6 năm 2020
  - maxsize: 20480 bytes, nếu tới max file name sẽ là log-31062020-hl1.log

## Utils core

sử dụng axios: https://www.npmjs.com/package/axios

core tương đương với axios. File core sẽ là nơi config hoặc setup những điều kiện cần để gọi core api hahalolo. Đông thời đây cũng là nơi write log chính sử dụng axios interceptors

defaul respond khi có err từ core:

```javascript
// line 44-48
{
  statusCode: 501,
  timestamp: new Date().toISOString(),
  message:
    'Chào mừng bạn đến với Hahalolo, API nay đang được bảo trì và nâng cấp, xin bạn thử lại sau!',
}

```

## Utils http-exception.filter

sử dụng Catch và ExceptionFilter từ nestjs

Đây là nơi handle respond HttpException của nestjs.

defaul respond cho tất cả các HttpException của nest js khi có lỗi có thẻ được custum ở đây:

defaul respond:

```javascript
// line 21-25
{
  statusCode: status,
  timestamp: new Date().toISOString(),
  message:
    'Chào mừng đến với Hahalolo, xin vui lòng liên hệ với chúng tôi để thuận tiện cho việc trao đổi dữ liệu. Xin cảm ơn!',
}
```

## Hướng dẫn sử trong trong quá trình develop

sử dụng CLI của nestjs để generate https://docs.nestjs.com/cli/usages#nest-generate

vd: nest g <schematic> <name> [options]

có thể return một một Promise<any> như ví dụ ở post.controller.ts

```javascript
// line 21-25
return core('/posts')
  .then(function(response) {
    return response.data;
  })
  .catch(e => e);
```

có thể tự hanđle err ở catch nếu muốn, không defauls sẽ đc kích hoạt.

chú ý: tương tự nếu sử dụng service

Về vẫn đề check data type và mỏe option: Xin vùi lòng tham khảo dữ liệu nestjs ở những mục hữu ích như Guards, Pipes, Filter, Middleware, etc
